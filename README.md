# getMumbleUserIPsFromLog

A program made to get a list of IP (v4) addresses for the users connected to the local Mumble server. This can be combined with Netcat to send this information to another server.

This can then be combined with a script on another server to whitelist IP addresses using an .htaccess file:

	#!/bin/bash

	echo '<RequireAny>'
	echo "Require ip 127.0.0.1"
	echo "Require ip 10.28.25.146"
	for i in $(nc <hostname of server running getMumbleUserIPsFromLog> 49153 | cut -d ':' -f1); do
		echo "Require ip $i" 
	done
	echo '</RequireAny>'

Note of course that .htaccess files do not scale. This can also create issues where if the .htaccess file is updated as a client attempts to connect then the server will throw an error.
