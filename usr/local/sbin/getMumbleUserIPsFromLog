#!/bin/bash

	# 1 - The serviceName variable is null.
	# 2 - Non-zero exit code retuned while getting service PID.
	# 3 - The PID of the service is not an integer.
	# 4 - Non-zero exit code returned hwile getting server logs.
	# 5 - Failed to trim extraneous columns from server log.
	# 6 - Proposed connection ID is null.
	# 7 - Proposed connection ID is not an integer.
	# 8 - Could not get username of authenticated user.

unset serviceName
serviceName='mumble-server.service'

if [ -z "$serviceName" ]; then

	echo "$0: The service name is null."
	exit 1
fi

unset servicePID
if ! servicePID="$(systemctl show "$serviceName" --value -p MainPID)"; then

	echo "$0: Non-zero exit code returned while getting service PID." >&2
	exit 2
fi

if ! [ "$servicePID" -eq "$servicePID" ]; then

	echo "$0: The PID for the service is not an integer." >&2
	exit 3
fi

unset serverLog
if ! serverLog="$(journalctl -o cat _PID="$servicePID")"; then

	echo "$0: Non-zero exit code returned while getting service logs." >&2
	exit 4
fi

unset serverLog_temp
if ! serverLog_temp="$(echo "$serverLog" | cut -d ' ' -f5-)"; then

	echo "$0: Failed to trim extraneous columns from server log." >&2
	exit 5
fi

set -f
IFS=$'\n'
unset serverLog
serverLog=($serverLog_temp)

unset serverLog_procCount
serverLog_procCount=0

unset connectionIDs
unset connectionIDs_state
unset connectionIDs_user
unset connectionIDs_IP

while [ "$serverLog_procCount" -lt "${#serverLog[*]}" ]; do

	unset serverLog_current
	serverLog_current="${serverLog[$serverLog_procCount]}"

	if [ "${serverLog_current:0:1}" == '<' ]; then

		unset serverLog_current_procCount
		serverLog_current_procCount=1

		unset propConnectionID

		while [ "$serverLog_current_procCount" -lt "${#serverLog_current}" ]; do

			if [ "${serverLog_current:$serverLog_current_procCount:1}" == : ]; then

				break

			else
				propConnectionID="${serverLog_current:1:$serverLog_current_procCount}"
			fi

			((serverLog_current_procCount++))
		done

		if [ -z "$propConnectionID" ]; then

			echo "$0: Proposed connection ID is null." >&2
			exit 6
		fi

		if ! [ "$propConnectionID" -eq "$propConnectionID" ] &> /dev/null; then

			echo "$0: Proposed connection ID is not an integer." >&2
			exit 7
		fi

		unset connectionIDs_procCount
		connectionIDs_procCount=0

		unset isDuplicate
		isDuplicate=0

		while [ "$connectionIDs_procCount" -lt "${#connectionIDs[*]}" ]; do

			if [ "$propConnectionID" == "${connectionIDs[$connectionIDs_procCount]}" ]; then

				isDuplicate=1
				break
			fi

			((connectionIDs_procCount++))
		done

		if [ "$isDuplicate" == 0 ]; then

			connectionIDs[${#connectionIDs[*]}]="$propConnectionID"
		fi

		unset serverLog_currentArray
		IFS=' '
		set -f
		serverLog_currentArray=($serverLog_current)

		# States
		# - disconnected
		# - authenticated
		# - new

		case "${serverLog_currentArray[1]}" in

			'Connection')
				if [ "${serverLog_currentArray[2]}" == 'closed:' ]; then
					connectionIDs_state[$propConnectionID]='disconnected'
				fi
				;;

			'New')
				if [ "${serverLog_currentArray[2]}" == 'connection:' ]; then
					connectionIDs_state[$propConnectionID]='new'
					connectionIDs_IP[$propConnectionID]="${serverLog_currentArray[3]}"
					connectionIDs_user[$propConnectionID]=''
				fi
				;;

			'Authenticated')
				connectionIDs_state[$propConnectionID]='authenticated'

				unset serverLog_current_procCount_previous
				serverLog_current_procCount_previous="$serverLog_current_procCount"

				while [ "$serverLog_current_procCount" -lt "${#serverLog_current}" ]; do

					if [ "${serverLog_current:$serverLog_current_procCount:1}" == \( ]; then

						break
					else
						((serverLog_current_procCount++))
					fi
				done

				if [ ! "${serverLog_current:$serverLog_current_procCount:1}" == \( ]; then

					echo "$0: Could not get username of authenticated user." >&2
					exit 8
				fi

				connectionIDs_user[$propConnectionID]="${serverLog_current:$(($serverLog_current_procCount_previous+1)):$(($serverLog_current_procCount-$(($serverLog_current_procCount_previous+1))))}"

				if [ "${connectionIDs_IP[$propConnectionID]}" == '' ]; then

					connectionIDs_IP[$propConnectionID]=Unknown
				fi
				;;

			'Disconnecting')
				if [ "${serverLog_currentArray[2]}" == 'ghost:' ]; then
					connectionIDs_state[$propConnectionID]='disconnected'
				fi
				;;

			'Timeout')
				connectionIDs_state[$propConnectionID]='disconnected'
				;;

			*)
				:
		esac
		unset propConnectionID
		unset connectionIDs_procCount
	fi

	unset serverLog_current

	((serverLog_procCount++))
done

unset connectionIDs_procCount
connectionIDs_procCount=0

while [ "$connectionIDs_procCount" -lt "${#connectionIDs[*]}" ]; do

	if [ "${connectionIDs_state[${connectionIDs[$connectionIDs_procCount]}]}" == 'authenticated' ]; then

		echo "${connectionIDs_IP[${connectionIDs[$connectionIDs_procCount]}]} ${connectionIDs_user[${connectionIDs[$connectionIDs_procCount]}]}"
	fi

	((connectionIDs_procCount++))
done
